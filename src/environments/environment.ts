// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA0PlCp9eYZf1Scs_f-FWDTEPx31EjSpPg",
    authDomain: "test-9fb38.firebaseapp.com",
    databaseURL: "https://test-9fb38.firebaseio.com",
    projectId: "test-9fb38",
    storageBucket: "test-9fb38.appspot.com",
    messagingSenderId: "289588660305",
    appId: "1:289588660305:web:3d605aacad155167"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

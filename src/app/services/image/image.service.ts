import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  images: Observable<any[]>;
  constructor(private db: AngularFirestore) { }

  get_images(uidUser, limit) {
    this.images = this.db.collection('images', ref => ref.where('owner', '==', uidUser)
      .orderBy('date', 'desc')
      .limit(limit)).valueChanges();
    console.log(this.images);
    return this.images;
  }
}

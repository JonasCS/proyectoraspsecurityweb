# RaspSecurity

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 7.3.7.

## Instalación de paquetes

run `npm install`

## Desplegar Servidor Local

Ejecutar `ng serve` y navegar hacia `http://localhost:4200/`. La aplicación se volverá a cargar automáticamente si cambia alguno de los archivos de origen.